/**
 * Created by HuKang on 2018/4/20.
 */
var $;
layui.config({
    base : "/office/static/js/"
}).use(['form','layer','jquery'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage;
    $ = layui.jquery;
    form.verify({
        username: function(value){ //value：表单的值、item：表单的DOM对象
            if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                return '用户名不能有特殊字符';
            }
            if(/(^\_)|(\__)|(\_+$)/.test(value)){
                return '用户名首尾不能出现下划线\'_\'';
            }
            if(/^\d+\d+\d$/.test(value)){
                return '用户名不能全为数字';
            }
        }
        //我们既支持上述函数式的方式，也支持下述数组的形式
        //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
        ,password: [
            /^[\S]{6,12}$/
            ,'密码必须6到12位，且不能出现空格'
        ]
    });
    /**
     * 添加用户ajax
     */
    form.on("submit(updateUser)",function(data){
        //是否添加过信息
        if(window.sessionStorage.getItem("updateUser")){
            addUserArray = JSON.parse(window.sessionStorage.getItem("updateUser"));
        }
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
            top.layer.close(index);
            $.ajax({
                url: '/user/update',
                type:"GET",
                data:$("#User").serialize(),
                dataType:"json",
                cache: false,
                success:function(rs){
                    if(rs.status == '200'){
                        top.layer.msg(rs.message);
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }else if(rs.status == '404'){
                        top.layer.msg(rs.message);
                        window.location.reload();
                    }else{
                        top.layer.msg(rs.message);
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }
                },
            });
        },1000);
        return false;
    })

})

//格式化时间
function formatTime(_time){
    var year = _time.getFullYear();
    var month = _time.getMonth()+1<10 ? "0"+(_time.getMonth()+1) : _time.getMonth()+1;
    var day = _time.getDate()<10 ? "0"+_time.getDate() : _time.getDate();
    var hour = _time.getHours()<10 ? "0"+_time.getHours() : _time.getHours();
    var minute = _time.getMinutes()<10 ? "0"+_time.getMinutes() : _time.getMinutes();
    return year+"-"+month+"-"+day+" "+hour+":"+minute;
}

