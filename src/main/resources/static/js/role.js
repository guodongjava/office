layui.config({
	base : "/static/js/"
}).use(['form','layer','jquery','laypage'],function(){
	var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		laypage = layui.laypage,
		$ = layui.jquery;
	//添加角色
	$(".roleAdd_btn").click(function(){
		var index = layui.layer.open({
			title : "添加角色",
			type : 2,
			content : "/view/system/addrole",
			success : function(layero, index){

			}
		})
		//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
		$(window).resize(function(){
			layui.layer.full(index);
		})
		layui.layer.full(index);
	})



	

	//批量删除
    $(".batchDel").click(function(){
        var $checkbox = $('.news_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.news_list tbody input[type="checkbox"][name="checked"]:checked');
        if($checkbox.is(":checked")){
            layer.confirm('确定删除选中的信息？',{icon:3, title:'提示信息'},function(index){
                var index = layer.msg('删除中，请稍候',{icon: 16,time:false,shade:0.8});
                setTimeout(function(){
                    $.ajax({
                        url: '/role/deleteList',
                        type:"POST",
                        data:$("#Role").serialize(),
                        dataType:"json",
                        cache: false,
                        success:function(rs){
                            if(rs.status == '200'){
                                top.layer.msg(rs.message);
                                window.location.reload();
                            }else{
                                top.layer.msg(rs.message)
                                window.location.reload();
                            }
                        },
                    });
                },1000);
            })
        }else{
            layer.msg("请选择需要删除的角色");
        }
    })

	//全选
	form.on('checkbox(allChoose)', function(data){
		var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
		child.each(function(index, item){
			item.checked = data.elem.checked;
		});
		form.render('checkbox');
	});


	//操作
    $("body").on("click",".fp_Menu",function(){  //编辑
        var _this = $(this);
        var id= _this.attr("value");
        var index = layui.layer.open({
            title : "分配菜单",
            type : 2,
            content : '/url/havaNotMenu?id='+id,
            success : function(layero, index){

            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function(){
            layui.layer.full(index);
        })
        layui.layer.full(index);

    });

	$("body").on("click",".role_edit",function(){  //编辑
        var _this = $(this);
        var id= _this.attr("value");
        var index = layui.layer.open({
            title : "用户修改",
            type : 2,
            content : '/role/selectById?id='+id,
            success : function(layero, index){

            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function(){
            layui.layer.full(index);
        })
        layui.layer.full(index);
	})

	
	$("body").on("click",".role_del",function(){  //删除
        var _this = $(this);
        layer.confirm('确定删除此信息？',{icon:3, title:'提示信息'},function(index){
            var _id= _this.attr("value")
            $.ajax({
                type:'GET',
                data:{id:_id},
                dataType:'json',
                url:'/role/delete',
                success:function (rs) {
                    if(rs.status =='200'){
                        layer.msg(rs.message);
                        _this.parents("tr").remove();
                        layer.close(index);
                    }else{
                        layer.msg(rs.message);
                        layer.close(index);
                    }
                }
            })


        });
	})
	function newsList(that){
		//渲染数据
		function renderDate(data,curr){
			var dataHtml = '';
			if(!that){
				currData = newsData.concat().splice(curr*nums-nums, nums);
			}else{
				currData = that.concat().splice(curr*nums-nums, nums);
			}
			if(currData.length != 0){
				for(var i=0;i<currData.length;i++){
					dataHtml += '<tr>'
			    	+'<td><input type="checkbox" name="checked" lay-skin="primary" lay-filter="choose"></td>'
			    	+'<td align="left">'+currData[i].newsName+'</td>'
			    	+'<td>'+currData[i].newsAuthor+'</td>';
			    	if(currData[i].newsStatus == "待审核"){
			    		dataHtml += '<td style="color:#f00">'+currData[i].newsStatus+'</td>';
			    	}else{
			    		dataHtml += '<td>'+currData[i].newsStatus+'</td>';
			    	}
			    	dataHtml += '<td>'+currData[i].newsLook+'</td>'
			    	+'<td><input type="checkbox" name="show" lay-skin="switch" lay-text="是|否" lay-filter="isShow"'+currData[i].isShow+'></td>'
			    	+'<td>'+currData[i].newsTime+'</td>'
			    	+'<td>'
					+  '<a class="layui-btn layui-btn-mini "><i class="iconfont icon-edit"></i> 编辑</a>'
					+  '<a class="layui-btn layui-btn-normal layui-btn-mini news_collect"><i class="layui-icon">&#xe600;</i> 收藏</a>'
					+  '<a class="layui-btn layui-btn-danger layui-btn-mini news_del" data-id="'+data[i].newsId+'"><i class="layui-icon">&#xe640;</i> 删除</a>'
			        +'</td>'
			    	+'</tr>';
				}
			}else{
				dataHtml = '<tr><td colspan="8">暂无数据</td></tr>';
			}
		    return dataHtml;
		} 
		
		//分页
		var nums = 13; //每页出现的数据量
		if(that){
			newsData = that;
		}
		laypage({
			cont : "page",
			pages : Math.ceil(newsData.length/nums),
			jump : function(obj){
				$(".news_content").html(renderDate(newsData,obj.curr));
				$('.news_list thead input[type="checkbox"]').prop("checked",false);
		    	form.render();
			}
		})
	}
})
