var $;
layui.config({
	base : "/office/static/js/"
}).use(['form','layer','jquery'],function(){
	var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		laypage = layui.laypage;
		$ = layui.jquery;


 	form.on("submit(addRole)",function(data){
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
            top.layer.close(index);
            $.ajax({
                url: '/role/add',
                type:"GET",
                data:$("#Role").serialize(),
                dataType:"json",
                cache: false,
                success:function(rs){
                    if(rs.status == '200'){
                        top.layer.msg(rs.message);
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }else if(rs.status == '404'){
                        top.layer.msg(rs.message);
                        window.location.reload();
                    }else{
                        top.layer.msg(rs.message);
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    }
                },
            });
        },1000);
 		return false;
 	})
	
})

//格式化时间
function formatTime(_time){
    var year = _time.getFullYear();
    var month = _time.getMonth()+1<10 ? "0"+(_time.getMonth()+1) : _time.getMonth()+1;
    var day = _time.getDate()<10 ? "0"+_time.getDate() : _time.getDate();
    var hour = _time.getHours()<10 ? "0"+_time.getHours() : _time.getHours();
    var minute = _time.getMinutes()<10 ? "0"+_time.getMinutes() : _time.getMinutes();
    return year+"-"+month+"-"+day+" "+hour+":"+minute;
}
