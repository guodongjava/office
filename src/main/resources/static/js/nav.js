var navs = [{
    "title" : "系统管理	",
    "icon" : "icon-computer",
    "href" : "",
    "spread" : true
},{
    "title" : "404页面",
    "icon" : "&#xe61c;",
    "href" : "page/404.html",
    "spread" : false
},{
    "title" : "系统基本参数",
    "icon" : "&#xe631;",
    "href" : "page/systemParameter/systemParameter.html",
    "spread" : false
},{
    "title" : "权限管理",
    "icon" : "&#xe61c;",
    "href" : "",
    "spread" : false,
    "children" : [
        {
            "title" : "用户管理",
            "icon" : "&#xe631;",
            "href" : "page/user/users.html",
            "spread" : false
        },
        {
            "title" : "角色管理",
            "icon" : "&#xe631;",
            "href" : "page/user/role.html",
            "spread" : false
        },
        {
            "title" : "权限管理",
            "icon" : "&#xe631;",
            "href" : "",
            "spread" : false
        },
        {
            "title" : "菜单管理",
            "icon" : "&#xe631;",
            "href" : "page/user/url.html",
            "spread" : false
        }
    ]
}]