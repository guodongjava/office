package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.common.SessionUser;
import edu.taru.office.pojo.Sign;

import edu.taru.office.service.SignService;
import edu.taru.office.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**考勤管理
 * Created by HuKang on 2018/5/15.
 */
@Controller
public class AttendanceController {

    @Autowired
    SignService signService;

    /**
     * 查询签到列表并签到
     * @return
     */
    @RequestMapping("/attendance/signInList")
    public String qyrAttendanceList(Model model,HttpSession session){
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        String uid = user.getUserid();
        List<Sign> list = signService.qryUserSignIn(Integer.valueOf(user.getUserid()));
        model.addAttribute("list",list);
        return "system/signIn";
    }


    /**
     * 查询签退列表并签退
     * @return
     */
    @RequestMapping("/attendance/signBackList")
    public String qyrAttendanceSignBack(Model model,HttpSession session){
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        String uid = user.getUserid();
        List<Sign> list = signService.qryUserSignBack(Integer.valueOf(uid));
        model.addAttribute("list",list);
        return "system/signBack";
    }

    /**
     * 签到
     * @param session
     * @return
     */
    @RequestMapping("/attendance/signIn")
    @ResponseBody
    public Object singIn(HttpSession session){
        JsonResult<String> jsonResult = null;
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        try {
            Sign sign = signService.selectSignIn(Integer.valueOf(user.getUserid()));
            if (sign !=null && sign.getAttendancestatus().equals("1")){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"该用户已签到");
            } else {
                signService.SignIn(Integer.valueOf(user.getUserid()));
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"签到成功");
            }
        }catch (Exception e){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"未知异常");
        }


        return jsonResult;
    }

    /**
     * 签退
     * @param session
     * @return
     */
    @RequestMapping("/attendance/signBack")
    @ResponseBody
    public Object singBack(HttpSession session){
        JsonResult<String> jsonResult = null;
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        try{
            Sign signIn = signService.selectSignIn(Integer.valueOf(user.getUserid()));
            Sign signBack = signService.selectSignBack(Integer.valueOf(user.getUserid()));
            if (signIn ==null){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"该用户还未签到请先签到");
            }else if (signBack !=null && signBack.getAttendancestatus().equals("1")){
                jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"该用户已签退");
            }else {
                signService.SignBack(Integer.valueOf(user.getUserid()));
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"签退成功");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonResult;
    }

    /**
     * 查询考勤信息
     * @param model
     * @return
     */
    @RequestMapping("/qry/attendanceList")
    public String qryAttendanceList(Model model){
        List<Sign> list = signService.selectList();
        model.addAttribute("list",list);
        return "system/attendance";
    }

}
