package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.pojo.Url;
import edu.taru.office.service.UrlService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 * 菜单列表类 用于对菜单的增删改查
 */
@Controller
public class UrlController {
    @Autowired
    UrlService urlService;
    /**
     * 查询所有菜单
     */
    @RequestMapping("/url/list")
    public String QryUrl(HttpServletRequest request, HttpServletResponse response){
        List<Url> list = urlService.selectList();
        request.setAttribute("list",list);
        return "system/url";
    }

    /**
     * 查询角色未拥有的菜单
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/url/havaNotMenu")
    public String havaNotMenu(HttpServletRequest request, HttpServletResponse response){
        Integer roleid = Integer.valueOf(request.getParameter("id"));
        List<Url> list = urlService.havaNotMenu(roleid);
        request.setAttribute("roleid",roleid);
        request.setAttribute("list",list);
        return "system/havaNotMenu";
    }

    /**
     * 查询菜单树
     * @return
     */
    @RequestMapping("/url/qryMenuTree")
    @ResponseBody
    public Object qryMenuTree(){
        JsonResult< List<Url>> jsonResult = null;
        try {
            List<Url> menuTree = urlService.qryMenuTree();
            jsonResult = new JsonResult<List<Url>>(Constants.STATUS_OK,"查询成功",menuTree);
        }catch(Exception e){
            e.printStackTrace();
            jsonResult = new JsonResult<List<Url>>(Constants.STATUS_ERROR,"查询失败");
        }
        return jsonResult;
    }

    /**
     * 查询角色已拥有的菜单
     *
     * @param
     * @return
     */
    @RequestMapping("/url/qryRoleExistTree")
    @ResponseBody
    public Object qryRoleExistTree(HttpServletRequest request){
        Integer id = Integer.valueOf(request.getParameter("roleid"));

        JsonResult< List<Url>> jsonResult = null;
        try {
            List<Url> roleExistTree = urlService.qryRoleExistTree(id);
            jsonResult = new JsonResult<List<Url>>(Constants.STATUS_OK,"查询成功",roleExistTree);
        }catch(Exception e){
            e.printStackTrace();
            jsonResult = new JsonResult<List<Url>>(Constants.STATUS_ERROR,"查询失败");
        }
        return jsonResult;
    }

    /**
     * 删除菜单
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/url/delete")
    @ResponseBody
    public Object  delUrl(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        JsonResult<String> jsonResult = null;
        try {
            urlService.deleteByPrimaryKey(id);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){

            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }


    /**
     * 根据id查询 菜单用作修改
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/url/selectById")
    public String selectByPrimaryKey(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        Url url = urlService.selectByPrimaryKey(id);
        request.setAttribute("url",url);
        return "system/updateUrl";
    }

    /**
     * 菜单修改对菜单名称是否重复进行判断
     * @param url
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/url/update")
    @ResponseBody
    public Object UpdateUser(Url url,HttpServletRequest request,HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            System.out.println(url.getUrlid()+"******************");
            int result = urlService.updateRepeat(url);
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"菜单名称已存在");
            }else{
                urlService.updateByPrimaryKeySelective(url);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"修改成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"修改失败");
        }
        return jsonResult;
    }

    /**
     * 对菜单进行批量删除
     * @param checked
     * @return
     */
    @RequestMapping("/url/deleteList")
    @ResponseBody
    public Object  deleteList(String [] checked){
        JsonResult<String> jsonResult = null;
        try {
            urlService.deleteListById(checked);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){

            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }

    @RequestMapping("/url/add")
    @ResponseBody
    public Object AddUser(Url url, HttpServletRequest request,HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            int result = urlService.selectRepeat(url.getTitle());
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"用户名已存在");
            }else{
                urlService.insertSelective(url);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"添加成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"添加失败");
        }
        return jsonResult;
    }

}
