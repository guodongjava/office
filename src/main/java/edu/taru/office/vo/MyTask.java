package edu.taru.office.vo;

import java.util.Date;

/**
 * 待办任务
 */
public class MyTask {
    /**
     * 业务关键Key，通常是业务主键
     */
    String  businessKey;
    /**
     * 工作流中流程的任务Id
     */
    String  taskId;

    /**
     * 发起人编号
     */

    String  ownerId;
    /**
     * 发起人
     */
    String  owner;

    /**
     * 流程的标题
     */

    String title;

    /**
     * 创建时间
     */
    Date createDate;



    /**
     * 审批的类型(请假、飞机、差旅 )，根据类型不同，跳转到不同额审批表单页面
     */
    String   category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }




    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }



}
