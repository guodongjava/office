package edu.taru.office.common;

import edu.taru.office.pojo.User;

import java.io.Serializable;

/**
 * 会话用户
 */
public class SessionUser implements Serializable {
    //登录用户ID
    String  userid;
    //登录用户名
    String  username;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 登录用户对象（刚开始没准备用这个User对象）
     */
    User user;



    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public SessionUser(){

    }
    public SessionUser( String  userid,  String  username){
        this.userid=userid;
        this.username=username;
    }
    public SessionUser( String  userid,  String  username,User user){
        this.userid=userid;
        this.username=username;
        this.user=user;
    }
    public SessionUser( Integer  userid,  String  username){
        this.userid=String.valueOf(userid);
        this.username=username;
    }

    public SessionUser( Integer  userid,  String  username,User user){
        this.userid=String.valueOf(userid);
        this.username=username;
        this.user=user;
    }

}
