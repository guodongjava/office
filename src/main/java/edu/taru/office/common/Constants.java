package edu.taru.office.common;

/**
 * 常量库
 */
public class Constants {
    /**
     * 前台登录用户SessionKey
     */
    public static final  String   LOGIN_USER_KEY="loginUserKey";

    /**
     * 后台登录用户SessionKey
     */
    public static final  String   LOGIN_ADMIN_KEY="loginAdminKey";



    public static final  String    VERIFY_CODE_KEY="verifyCodeKey";

    /**
     * 登录错误消息
     */
    public static final String     LOGIN_ERROR_KEY="loginErrorKey";
    /**
     * 成功状态码
     */
    public static  final String   STATUS_OK ="200";
    public static  final  String  STATUS_NO_DATA="404";
    public static  final String   STATUS_ERROR ="500";

    public static  final String   ERROR ="error";
    public static  final String   SUCCESS ="success";
    public static  final String   FAILURE ="failure";

    public  static class Process {
        public static  final String   BUSINESS_KEY ="businessKey";
        public static  final  String  CATEGORY="category";
        public static  final String   TITLE ="title";
        public static  final String   OWNER ="owner";

    }

}
