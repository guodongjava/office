package edu.taru.office.common;

import java.io.Serializable;

/**
 * 统一的响应Json结果，前端所有的json都要统一这个格式
 * code 是状态码，状态码的标准类采用Http响应码
 * 200表示成功 404 表示没有数据  500表示出现异常
 */
public class JsonResult<T>  implements Serializable {
    /**
     * 响应的数据
     */
    private T  data;
    /**
     * 响应状态码
     */
    private String status;
    /**
     * 响应消息
     */
    private String message;

    public  JsonResult(){

    }
    public  JsonResult(String status,String message ){
        this.status=status;
        this.message=message;

    }

    public  JsonResult(String status,String message ,T data){
        this.status=status;
        this.message=message;
        this.data=data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
