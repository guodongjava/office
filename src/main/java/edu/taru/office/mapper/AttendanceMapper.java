package edu.taru.office.mapper;

import edu.taru.office.pojo.Attendance;
import edu.taru.office.pojo.Sign;

import java.util.HashMap;
import java.util.List;

public interface AttendanceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Attendance record);

    int insertSelective(Attendance record);

    Attendance selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Attendance record);

    int updateByPrimaryKeyWithBLOBs(Attendance record);

    int updateByPrimaryKey(Attendance record);

    /**
     * 查询用户签到信息
     * @param uid
     * @return
     */
    List<HashMap> qryUserSignIn(Integer uid);
    /**
     * 查询用户签退信息
     * @param uid
     * @return
     */
    List<HashMap> qryUserSignBack(Integer uid);

    /**
     * 判断是否已签到
     * @param uid
     * @return
     */
    HashMap selectSignIn(Integer uid);

    /**
     * 签到
     * @param uid
     */
    void SignIn(Integer uid);

    /**
     * 签退
     * @param uid
     */
    void SignBack(Integer uid);



}