package edu.taru.office.mapper;

import edu.taru.office.pojo.Url;
import edu.taru.office.pojo.UserInfo;

import java.util.List;

public interface UrlMapper {
    int deleteByPrimaryKey(Integer urlid);

    int insert(Url record);

    int insertSelective(Url record);

    List<Url> selectList();

    Url selectByPrimaryKey(Integer urlid);

    int updateByPrimaryKeySelective(Url record);

    int updateByPrimaryKey(Url record);

    /**
     * 查询菜单
     * @param username
     * @return
     */
    List<Url> selectMenusByUserName(String username);

    /**
     * 查询角色未拥有的菜单
     * @param roleid
     * @return
     */
    List<Url> havaNotMenu(Integer roleid);

    /**
     * 查询菜单树
     * @return
     */
    List<Url> qryMenuTree();

    /**
     * 查询角色已拥有的菜单树
     * @return
     */
    List<Url> qryRoleExistTree(Integer roleid);

    /**
     * 菜单修改是对菜单名称是否重复进行判断
     * @param url
     * @return
     */
    int updateRepeat(Url url);

    /**
     * 添加菜单是判断菜单名称是否存在
     * @param title
     * @return
     */
    int selectRepeat(String title);


}