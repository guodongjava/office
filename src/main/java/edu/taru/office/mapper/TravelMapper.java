package edu.taru.office.mapper;

import edu.taru.office.pojo.Travel;
import org.apache.ibatis.annotations.Param;

public interface TravelMapper {
    int deleteByPrimaryKey(Long id);

    Integer insert(Travel record);

    int insertSelective(Travel record);

    Travel selectByPrimaryKey(String id);

    int  updateResultByKey(@Param("result") String result,@Param("id") Integer id);
}