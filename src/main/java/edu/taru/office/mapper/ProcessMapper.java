package edu.taru.office.mapper;

import java.util.List;

public interface ProcessMapper {
    /**
     * 根据用户ID查询角色ID
     * @return
     */
    public List<String> selectRoleIdsByUserId(Integer id);
 }
