package edu.taru.office.mapper;

import edu.taru.office.pojo.User;
import edu.taru.office.pojo.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectList();

    User selectByPrimaryKey(Integer id);

    /**
     * 认证查询
     * @param temp
     * @return
     */
    User selectByAuthc(User temp);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 根据用户id查询 用户未拥有的角色做分配权限使用
     * @param userid
     * @return
     */
    List<HashMap> selectRoleByUserId(Integer userid);

    /**
     * 给用户添加权限（角色）
     * @param userid
     * @param roleid
     * @return
     */
    void insertRole(@Param("userid") Integer userid,@Param("roleid") Integer roleid);

    /**
     * 根据用户id查询用户已有角色
     * @param userid
     * @return
     */
    List<HashMap> qryExistRole(Integer userid);

    /**
     * 添加用户时判断用户名是否存在
     * @param username
     * @return
     */
    int selectRepeat(String username);

    /**
     * 用户修改时判断修改的用户名是否存在，如果未修改用户名则可以存在
     * @param user
     * @return
     */
    int updateRepeat(User user);

    /**
     * 向用户详细表中插入数据
     * @param userinfo
     * @return
     */
    int insertUserinfo(UserInfo userinfo);

    /**
     * 查询用户详细信息
     * @param id
     * @return
     */
    HashMap<String ,Object> qryUserInfo(Integer id);

    /**
     * 给主页面查询用户基本信息
     * @param id
     * @return
     */
    HashMap<String,Object> qryUserForMain(Integer id);

}