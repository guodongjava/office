package edu.taru.office.mapper;

import edu.taru.office.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer roleid);

    int insert(Role record);

    /**
     *添加角色是判断角色名称是否存在
     * @param rolename
     * @return
     */
    int selectRepeat(String rolename);

    /**
     * 角色修改判断角色名称是否存在，如果是当前的则可以提交，否则角色名称已存在
     * @param role
     * @return
     */
    int updateRepeat(Role role);
    /**
     * 添加角色
     * @param record
     * @return
     */
    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleid);

    List<Role> selectList();

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    /**
     * 给角色添加菜单
     * @param roleid
     * @param urlid
     */
    void insertUrl(@Param("roleid") Integer roleid, @Param("urlid") Integer urlid);

}