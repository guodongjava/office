package edu.taru.office.service.impl;

import edu.taru.office.mapper.TravelMapper;
import edu.taru.office.pojo.Travel;
import edu.taru.office.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 差旅管理服务实现
 */
@Service
public class TravelServiceImpl implements TravelService {

    @Autowired
    TravelMapper  travelMapper;


    @Override
    public void insertTravel(Travel travel) {
        travelMapper.insert(travel);
    }

    @Override
    public Travel selectTravel(String busId) {
        return travelMapper.selectByPrimaryKey(busId);
    }


    @Override
    public int updateResultByKey(String result, String id) {
        return travelMapper.updateResultByKey(result,Integer.valueOf(id));
    }
}
