package edu.taru.office.service.impl;

import edu.taru.office.mapper.LeaveMapper;
import edu.taru.office.pojo.Leave;
import edu.taru.office.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by HuKang on 2018/5/5.
 */
@Service
public class LeaveServiceImpl implements LeaveService{
    @Autowired
    LeaveMapper leaveMapper;

    @Override
    public int insert(Leave record) {
        return leaveMapper.insertSelective(record);
    }

    @Override
    public Leave selectLeave(String busid) {
        return leaveMapper.selectByPrimaryKey(Integer.valueOf(busid));
    }

    @Override
    public int updateResultByKey(String result, String id) {
        return leaveMapper.updateResultByKey(result,Integer.valueOf(id));
    }

    @Override
    public Leave selectByPrimaryKey(Integer id) {
        return leaveMapper.selectByPrimaryKey(id);
    }
}
