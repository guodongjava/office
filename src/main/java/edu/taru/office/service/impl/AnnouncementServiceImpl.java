package edu.taru.office.service.impl;

import edu.taru.office.mapper.AnnouncementMapper;
import edu.taru.office.pojo.Announcement;
import edu.taru.office.service.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HuKang on 2018/5/6.
 */
@Service
public class AnnouncementServiceImpl implements AnnouncementService {
    @Autowired
    AnnouncementMapper announcementMapper;

    @Override
    public List<Announcement> selectList() {
        return announcementMapper.selectList();
    }

    @Override
    public List<Announcement> selectLimitShow() {
        return announcementMapper.selectLimitShow();
    }

    @Override
    public Announcement selectByPrimaryKey(Integer id) {
        return announcementMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insertAnnouncement(String title, String content) {
        return announcementMapper.insertAnnouncement(title,content);
    }

    @Override
    public void deleteAnnouncement(String[] id) {
        for (int i=0 ;i<id.length; i++){
            announcementMapper.deleteByPrimaryKey(Integer.valueOf(id[i]));
        }
    }
}
