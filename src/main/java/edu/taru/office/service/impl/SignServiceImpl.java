package edu.taru.office.service.impl;

import edu.taru.office.mapper.SignMapper;
import edu.taru.office.pojo.Sign;
import edu.taru.office.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HuKang on 2018/5/15.
 */
@Service
public class SignServiceImpl implements SignService {
    @Autowired
    SignMapper signMapper;

    @Override
    public List<Sign> qryUserSignIn(Integer uid) {
        return signMapper.qryUserSignIn(uid);
    }

    @Override
    public Sign selectSignIn(Integer uid) {
        return signMapper.selectSignIn(uid);
    }

    @Override
    public void SignIn(Integer uid) {
        signMapper.SignIn(uid);
    }

    @Override
    public List<Sign> qryUserSignBack(Integer uid) {
        return signMapper.qryUserSignBack(uid);
    }

    @Override
    public Sign selectSignBack(Integer uid) {
        return signMapper.selectSignBack(uid);
    }

    @Override
    public void SignBack(Integer uid) {
        signMapper.SignBack(uid);
    }

    @Override
    public List<Sign> selectList() {
        return signMapper.selectList();
    }
}
