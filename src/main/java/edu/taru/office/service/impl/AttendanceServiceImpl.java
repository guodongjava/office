package edu.taru.office.service.impl;

import edu.taru.office.mapper.AttendanceMapper;
import edu.taru.office.pojo.Attendance;
import edu.taru.office.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by HuKang on 2018/5/15.
 */
@Service
public class AttendanceServiceImpl implements AttendanceService {
    @Autowired
    AttendanceMapper attendanceMapper;

    @Override
    public List<HashMap> qryUserSignIn(Integer uid) {
        return attendanceMapper.qryUserSignIn(uid);
    }

    @Override
    public List<HashMap> qryUserSignBack(Integer uid) {
        return attendanceMapper.qryUserSignBack(uid);
    }

    @Override
    public void SignBack(Integer uid) {
        attendanceMapper.SignBack(uid);
    }

    @Override
    public void SignIn(Integer uid) {
        attendanceMapper.SignIn(uid);
    }

    @Override
    public HashMap selectSignIn(Integer uid) {
        return attendanceMapper.selectSignIn(uid);
    }
}
