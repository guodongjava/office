package edu.taru.office.service;

import edu.taru.office.pojo.Role;

import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 * 角色类
 */
public interface RoleService {
    /**
     * 查询所有角色
     */
    List<Role> selectList();

    /**
     * 添加角色
     * @param record
     * @return
     */
    int insertSelective(Role record);

    /**
     *添加角色是判断角色名称是否存在
     * @param rolename
     * @return
     */
    int selectRepeat(String rolename);

    /**
     * 删除角色
     * @param roleid
     * @return
     */
    int deleteByPrimaryKey(Integer roleid);

    /**
     * 根据角色id去查询用户
     * @param roleid
     * @return
     */
    Role selectByPrimaryKey(Integer roleid);

    /**
     * 角色修改判断角色名称是否存在，如果是当前的则可以提交，否则角色名称已存在
     * @param role
     * @return
     */
    int updateRepeat(Role role);

    /**
     * 角色修改
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Role record);

    /**
     * 根据roleid 批量删除
     * @param roleid
     * @return
     */
    void  deleteByPrimaryKey(String [] checked);

    /**
     * 给角色添加菜单
     * @param roleid
     * @param checked
     */
    void insertUrl(String roleid ,String [] checked );
}
