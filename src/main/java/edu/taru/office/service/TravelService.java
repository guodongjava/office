package edu.taru.office.service;

import edu.taru.office.pojo.Travel;

public interface TravelService {

    /**
     * 添加一个差旅单
     * @param travel
     */
    void  insertTravel(Travel travel);

    /**
     * 查询一个差旅单，根据id
     * @param busId
     * @return
     */
    Travel selectTravel(String  busId);


    /**
     * 审批结束后，更新审批意见
     * @param result
     * @param id
     * @return
     */
    int  updateResultByKey(String result,String id);
}
