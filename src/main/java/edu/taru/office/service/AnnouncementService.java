package edu.taru.office.service;

import edu.taru.office.pojo.Announcement;

import java.util.List;

/**
 * Created by HuKang on 2018/5/6.
 */
public interface AnnouncementService {
    /**
     * 查询公告列表
     * @return
     */
    List<Announcement> selectList();
    /**
     * 首页展示公告（分页）
     * @return
     */
    List<Announcement> selectLimitShow();

    /**
     * 查询公告详情
     * @param id
     * @return
     */
    Announcement selectByPrimaryKey(Integer id);

    /**
     * 添加公告
     * @param title
     * @param content
     * @return
     */
    int insertAnnouncement(String title,String content);

    /**
     * 批量删除公告
     * @param id
     */
    void deleteAnnouncement(String [] id);
}
