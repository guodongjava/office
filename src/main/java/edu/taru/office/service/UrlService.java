package edu.taru.office.service;

import edu.taru.office.pojo.Url;

import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 * 菜单接口
 */
public interface UrlService {
    /**
     * 查询所有菜单
     */
    List<Url> selectList();

    /**
     * 查询角色未拥有的菜单
     * @param roleid
     * @return
     */
    List<Url> havaNotMenu(Integer roleid);

    /**
     * 查询菜单树
     * @return
     */
    List<Url> qryMenuTree();

    /**
     * 查询角色已拥有的菜单树
     * @return
     */
    List<Url> qryRoleExistTree(Integer roleid);

    /**
     * 删除菜单
     * @param urlid
     * @return
     */
    int deleteByPrimaryKey(Integer urlid);

    /**
     * 根据urlid 查询菜单用作修改
     * @param urlid
     * @return
     */
    Url selectByPrimaryKey(Integer urlid);

    /**
     * 菜单修改是对菜单名称是否重复进行判断
     * @param url
     * @return
     */
    int updateRepeat(Url url);

    /**
     * 菜单修改
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Url record);

    /**
     * 根据urlid批量删除url
     * @param id
     * @return
     */
    void deleteListById(String [] id);

    /**
     * 添加菜单是判断菜单名称是否存在
     * @param title
     * @return
     */
    int selectRepeat(String title);

    /**
     * 添加菜单
     * @param record
     * @return
     */
    int insertSelective(Url record);
}
