package edu.taru.office.util;

import edu.taru.office.common.Constants;
import edu.taru.office.common.SessionUser;
import edu.taru.office.pojo.User;
import org.apache.shiro.session.Session;

import javax.servlet.http.HttpSession;

/**
 * 登录用户工具类
 */
public class UserUtil {

    /**
     * 获取登录信息
     * @param session
     * @return
     */
    public static SessionUser  getSessionUserFromSession(HttpSession session){
        SessionUser  sessionUser =(SessionUser)session.getAttribute(Constants.LOGIN_USER_KEY);
        return  sessionUser;
    }


    /**
     * 获取登录信息
     * @param session
     * @return
     */
    public  static User  getUserFromSession(HttpSession session){
        SessionUser  sessionUser =(SessionUser)session.getAttribute(Constants.LOGIN_USER_KEY);
        User user =sessionUser.getUser();
        return  user;
    }



}
