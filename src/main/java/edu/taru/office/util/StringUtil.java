package edu.taru.office.util;

public class StringUtil {

    public static  String  toNullOrString(Object object){
        return object==null?null:object.toString();
    }

    public static  String  toEmptyOrString(Object object){
        return object==null?"":object.toString();
    }


}
